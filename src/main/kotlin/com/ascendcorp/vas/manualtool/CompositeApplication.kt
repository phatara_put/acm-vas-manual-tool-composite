package com.ascendcorp.vas.manualtool

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CompositApplication

fun main(args: Array<String>) {
	runApplication<CompositApplication>(*args)
}
